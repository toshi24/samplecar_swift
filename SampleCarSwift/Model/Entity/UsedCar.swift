//
//  UsedCar.swift
//  SampleCarSwift
//
//  Created by Toshiyuki Nishi on 2016/01/21.
//  Copyright © 2016年 Toshiyuki Nishi. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct UsedCar: CustomStringConvertible {
    public var id: String
    public var name: String
    public var photoUrl: String
    public var brandName: String
    public var model: String
    public var grade: String
    public var price: String
    
    public var description: String {
        get {
            var string = "\nId: \(id)\n"
            string += "\nName: \(name)\n"
            string += "\nPhotoUrl: \(photoUrl)\n"
            string += "\nbrandName: \(brandName)\n"
            string += "\nmodel: \(model)\n"
            string += "\ngrade: \(grade)\n"
            string += "\nprice: \(price)\n"
            return string
        }
    }

    public init(object: JSON) {
        self.id = object["id"].string!
        self.name = object["body"]["name"].string!
        self.photoUrl = object["photo"]["main"]["s"].string!
        self.brandName = object["brand"]["name"].string!
        self.model = object["model"].string!
        self.grade = object["grade"].string!
        
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.decimal
        formatter.groupingSeparator = ","
        formatter.groupingSize = 3
        
        if let price = object["price"].number,
            let priceStrint = formatter.string(from:price) {
            self.price = "本体価格 : " + priceStrint + "円"
        } else {
            self.price = "本体価格 : -- "
        }
    }
    
    static func collection(object: JSON) -> [UsedCar] {
        return object.arrayValue.map { UsedCar(object: $0) }
    }

}
