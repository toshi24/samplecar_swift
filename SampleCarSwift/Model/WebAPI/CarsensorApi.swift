//
//  CarsensorApi.swift
//  SampleCarSwift
//
//  Created by Toshiyuki Nishi on 2016/01/21.
//  Copyright © 2016年 Toshiyuki Nishi. All rights reserved.
//

import Foundation

import Alamofire
import SwiftyJSON
import SVProgressHUD

open class CarsensorApi {

    // 読込完了Notification
    open let SCSLoadCompleteNotification = "SCSLoadCompleteNotification"

    // Carsensor webservice api key
    let apiKey = "(ここにAPIキー)"
    
    // Carsensor webservice url
    let apiUrl = "https://webservice.recruit.co.jp/carsensor/usedcar/v1/"
    
    // 中古車情報配列
    open var usedCars = [UsedCar]()
    
    // 読み込み中フラグ
    var loading = false

    // 検索の開始位置
    var start = 1

    // データ総数
    open var total = 0
    
    // 検索条件
    var condition: SearchCondition = SearchCondition() {
        didSet {
            usedCars = []
        }
    }
    
    public init(){}
    
    public init(condition: SearchCondition){ self.condition = condition }
    
    open func searchUsedCar(_ reset: Bool = false) {
        
        if loading {
            return
        }
        
        if reset {
            usedCars = []
            start = 1
        } else {
            start += usedCars.count
        }

        loading = true
        
        var params = condition.queryParams
        params["key"] = apiKey
        params["start"] = String(start)
        params["format"] = "json"

        // ローディング表示
        SVProgressHUD.show()
        
        Alamofire.request(apiUrl, method:.get, parameters:params)
            .responseJSON { response in
                // ローディング非表示
                SVProgressHUD.dismiss()
                
                switch response.result {
                case .success(let value):
                    self.loading = false
                    
                    let json = JSON(value)
                    
                    if let errors = json["results"]["error"].array {
                        // API実行終了を通知する
                        self.notifyComplete(errors[0]["message"].string!)
                        return
                    }

                    self.total = json["results"]["results_available"].int!

                    self.usedCars += UsedCar.collection(object: json["results"]["usedcar"])
                    
                    // API実行終了を通知する
                    self.notifyComplete()
                    
                case .failure(let error):
                    self.loading = false
                    
                    // API実行終了を通知する
                    self.notifyComplete(error.localizedDescription)
                }
        }
    }

    // TODO: Notification用のutilを作って移動させたい
    // API実行終了を通知する
    private func notifyComplete(_ errorMessage: String? = nil) {
        var userInfo: [String: String] = [:]

        if let errorMessage = errorMessage {
            userInfo = ["error": errorMessage]
        }
        
        NotificationCenter.default.post(
            name: Notification.Name(rawValue: self.SCSLoadCompleteNotification),
            object: nil,
            userInfo: userInfo)
    }
    
}
