//
//  CarListItemTableViewCell.swift
//  SampleCarSwift
//
//  Created by Toshiyuki Nishi on 2016/01/19.
//  Copyright © 2016年 Toshiyuki Nishi. All rights reserved.
//

import UIKit

class CarListItemTableViewCell: UITableViewCell {

    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var brand: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var price: UILabel!
    
    var usedCar:UsedCar? = nil {
        didSet {
            // URLがあれば画像を表示する
            if let url = usedCar?.photoUrl {
                photo.sd_cancelCurrentAnimationImagesLoad()
                photo.sd_setImage(
                    with: URL(string: url),
                    placeholderImage: UIImage(named: "loading"),
                    options: .retryFailed)
            }
            
            brand.text = (usedCar?.brandName)!
            name.text = (usedCar?.model)! + " " + (usedCar?.grade)!
            price.text = (usedCar?.price)!
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
