# README #

カーセンサーnet Webサービスを使用して中古車情報を検索、閲覧するアプリケーションです。

* 使用言語：Swift
* iOS:8.0以降
* cocoapods:1.2.1
* Build確認したXcode：ver8.3.2
* ライブラリ：Alamofire, SDWebImage

[![カーセンサーnet Webサービス](http://webservice.recruit.co.jp/banner/car-m.gif)](http://webservice.recruit.co.jp/)

## Build方法 ##

* リクルートWebサービスでAPIキーを発行してもらいます。
  https://webservice.recruit.co.jp/register/index.html
* 発行してもらったAPIキーをソースファイル(CarsensorApi.swift)の以下の箇所に書き込みます。

```
    // Carsensor webservice api key  
    let apiKey = "(ここにAPIキー)"  
```
* ターミナルを開き、プロジェクトフォルダ直下まで移動して以下のコマンドを実行します。

```
   pod install  
```

* プロジェクトフォルダ直下のSampleCarSwift.xcworkspaceをXcodeで開きます。
* Xcodeのメニュー "Product" -> "Build"でBuildします。


## 実行イメージ ##
![実行イメージ](http://toshe.sunnyday.jp/swift_demo/samplecar.gif)