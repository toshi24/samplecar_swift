//
//  SearchCondition.swift
//  SampleCarSwift
//
//  Created by Toshiyuki Nishi on 2016/01/21.
//  Copyright © 2016年 Toshiyuki Nishi. All rights reserved.
//

import Foundation

public struct SearchCondition: CustomStringConvertible {
    public var keyword: String? = nil
    public var pref: String? = nil
    
    public var description: String {
        get {
            var string = "\nKeyword: \(keyword)\n"
            string += "\nPref: \(pref)\n"
            return string
        }
    }
    
    public var queryParams: [String: String] {
        get {
            var params = [String: String]()
            
            if let keyword = keyword {
                params["keyword"] = keyword
            }
            
            if let pref = pref {
                params["pref"] = pref
            }

            return params
        }
    }
}