
//
//  CarListViewController.swift
//  SampleCarSwift
//
//  Created by Toshiyuki Nishi on 2016/01/19.
//  Copyright © 2016年 Toshiyuki Nishi. All rights reserved.
//

import UIKit

class CarListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var carsensorApi = CarsensorApi()
    
    var loadDataObserver: NSObjectProtocol?
    var refreshObserver: NSObjectProtocol?

    
    // MARK: - life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(CarListViewController.onRefresh(_:)), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // 読込完了通知を受信した時の処理
        loadDataObserver = NotificationCenter.default.addObserver(
            forName: NSNotification.Name(rawValue: carsensorApi.SCSLoadCompleteNotification),
            object: nil,
            queue: nil,
            using: {
                (notification) in
                
                if notification.userInfo != nil {
                    if let userInfo = notification.userInfo as? [String:String] {
                        if userInfo["error"] != nil {
                            // アラートメッセージを表示
                            self.showAlert("エラー", message:userInfo["error"]!)
                            return
                        }
                    }
                }
                self.tableView.reloadData()
            }
        )

        // データ取得
        getUsedCars()
    }

    override func viewWillDisappear(_ animated: Bool) {
        // 通知の待ち受けを終了
        NotificationCenter.default.removeObserver(self.loadDataObserver!)
    }

    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return carsensorApi.usedCars.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if indexPath.row < carsensorApi.usedCars.count {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CarListItem", for: indexPath) as! CarListItemTableViewCell
                cell.usedCar = carsensorApi.usedCars[indexPath.row]
                
                if carsensorApi.usedCars.count < carsensorApi.total {
                    if carsensorApi.usedCars.count - indexPath.row <= 4 {
                        carsensorApi.searchUsedCar()
                    }
                }
                
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

    // MARK: - private method

    // データ取得
    func getUsedCars() {
        var condition = SearchCondition()
        condition.pref = "13"
        
        carsensorApi.condition = condition
        carsensorApi.searchUsedCar(true)
    }
    
    // pull to refresh method
    func onRefresh(_ refreshControl: UIRefreshControl) {
        refreshControl.beginRefreshing()
        refreshObserver = NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: carsensorApi.SCSLoadCompleteNotification), object: nil, queue: nil, using: {
                notification in
                NotificationCenter.default.removeObserver(self.refreshObserver!)
            refreshControl.endRefreshing()
        })
        carsensorApi.searchUsedCar(true)
    }
    
    // アラートメッセージを表示
    func showAlert(_ title: String,  message mes:String) {
        let alertController = UIAlertController(
            title:title,
            message:mes,
            preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title:"OK", style: .default) {
            action in return
            }
        )
        self.present(alertController, animated:true, completion:nil)
    }

}
